\documentclass[11pt, aspectratio=1610]{beamer}
\usetheme{Warsaw}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{tikz}
\usetikzlibrary{arrows,arrows.meta}
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage{setspace}
\usepackage{todonotes}
\presetkeys{todonotes}{inline}{}
\renewcommand{\baselinestretch}{1.25}

\definecolor{orange_garage}{RGB}{255,147,41}
\definecolor{gris_garage}{RGB}{78,78,78}
\definecolor{rose_deuxfleurs}{RGB}{234,89,110}

\author[Association Deuxfleurs]{~\linebreak Vincent Giraud}
\title[Remettre internet sur la place publique]{Remettre internet sur la place publique}
\subtitle{L'entre-hébergement avec Garage}
%\setbeamercovered{transparent} 
%\setbeamertemplate{navigation symbols}{} 
\date{Pas Sage En Seine 2023\linebreak

\scriptsize Vendredi 16 juin 2023\linebreak
} 

\setbeamercolor{palette primary}{fg=black,bg=rose_deuxfleurs}
\setbeamercolor{palette secondary}{fg=gris_garage,bg=gris_garage}
\setbeamercolor{palette tiertary}{fg=white,bg=gris_garage}
\setbeamercolor{palette quaternary}{fg=white,bg=gris_garage}
\setbeamercolor{navigation symbols}{fg=black, bg=white}
\setbeamercolor{navigation symbols dimmed}{fg=darkgray, bg=white}
\setbeamercolor{itemize item}{fg=gris_garage}
\setbeamertemplate{itemize item}[circle]
\setbeamercolor{block body}{bg=rose_deuxfleurs!33}

\setbeamertemplate{navigation symbols}{}
\addtobeamertemplate{navigation symbols}{}{%
    \usebeamerfont{footline}%
    \usebeamercolor[fg]{footline}%
    \hspace{1em}%
    \insertframenumber/\inserttotalframenumber
}

\setbeamertemplate{headline}
{%
  \leavevmode%
  \begin{beamercolorbox}[wd=.5\paperwidth,ht=2.5ex,dp=1.125ex]{section in head/foot}%
    \hbox to .5\paperwidth{\hfil\insertsectionhead\hfil}
  \end{beamercolorbox}%
  \begin{beamercolorbox}[wd=.5\paperwidth,ht=2.5ex,dp=1.125ex]{subsection in head/foot}%
    \hbox to .5\paperwidth{\hfil\insertsubsectionhead\hfil}
  \end{beamercolorbox}%
}
\addtobeamertemplate{footnote}{}{\vspace{2ex}}

\begin{document}
\begin{frame}
\titlepage
\end{frame}

\section{Introduction}
\subsection{Présentation}
\begin{frame}
\begin{columns}
\column{0.5 \linewidth}
\begin{center}
\includegraphics[width=3.5cm]{ressources/deuxfleurs-logo.png}\linebreak

\texttt{https://deuxfleurs.fr}
\end{center}
\column{0.4 \linewidth}
\begin{center}
Deuxfleurs est une association militant en faveur d'un internet plus convivial, avec une organisation et des rapports de force repensés.\linebreak

Nous faisons partie du CHATONS\footnote[frame]{Collectif des Hébergeurs Alternatifs, Transparents, Ouverts, Neutres et Solidaires} depuis avril 2022.

\includegraphics[width=2cm]{ressources/logo_chatons.png}
\end{center}
\end{columns}
\end{frame}

\subsection{Concentration de l'internet}
\begin{frame}
  \centering
  Les ressources accessibles par internet reposent aujourd'hui essentiellement dans des centres de données.

  \vspace{0.75cm}

  \begin{columns}
    \column{0.6 \linewidth}
    \centering
    \includegraphics[width=8cm]{ressources/centre de données.png}
    \column{0.4 \linewidth}
    \centering
    Cette concentration répond aux logiques industrielles et commerciales solidement implantées dans le domaine du numérique.
  \end{columns}
\end{frame}

\section{Conséquences de la concentration de l'internet}
\subsection{Contrôle sur les ressources}
\begin{frame}
  \centering
  Cet état de fait entraîne d'abord un contrôle monumental sur les ressources en question.

  \vspace{0.75cm}

  \begin{columns}
    \column{0.5 \linewidth}
    \centering
    \onslide<2->{
      Pour les entreprises et groupes privés qui peuvent les valoriser notamment~:
      \begin{itemize}
      \item via un ciblage publicitaire
      \item pour entraîner des intelligences artificielles
      \item pour maintenir leur position concurentielle
      \end{itemize}
    }
    \column{0.5 \linewidth}
    \centering
    \onslide<3->{
      Pour les États, qui y trouvent des points d'entrée privilégiés où piocher dans le cadre de surveillance de masse\footnote[frame]{\onslide<3->{Cf. les révélations d'Edward Snowden, en particulier ici le programme PRISM}}\linebreak
      
      \includegraphics[height=2.5cm]{ressources/panoptique.png}
    }
  \end{columns}
\end{frame}

\subsection{L'invisibilisation}
\begin{frame}
  \centering
  Dans l'industrie, la part des ressources humaines intéragissant avec les centres de données est minimisée autant que possible.

  \vfill
  
  Mais l'invisibilisation des ressources matérielles touche d'abord la société publique\footnote[frame]{Sondage réalisé par Wakefield Research pour Citrix, sur 1000 États-Uniens}~:
  
  \begin{itemize}[<+(1)->]
  \item 16\% des États-Uniens savent réellement ce que signifie le \textit{cloud}
  \item 22\% des États-Uniens font semblant de savoir ce dont il s'agit au quotidien
  \item 29\% des États-Uniens pensent que le \textit{cloud} utilise des nuages
  \item 51\% des États-Uniens pensent qu'une mauvaise météo influe sur ses performances
  \end{itemize}

  \vspace{0.5cm}

  \onslide<6->{Cette abstraction du matériel derrière le concept d'internet est déterminant dans les comportements de chacun.}
\end{frame}

\subsection{Le clientélisme}
\begin{frame}
  \centering
  La mutation d'internet à travers le prisme commercial divise les acteurs en deux~:\\ les vendeurs, et les clients.

  \vspace{0.5cm}

  \begin{block}{}
    \scriptsize
    \textit{La définition industrielle des valeurs rend extrêmement difficile à l'usager de percevoir la structure profonde des moyens sociaux. (...) Il a du mal à imaginer que l'on puisse gagner en rendement social ce que l'on perd en rentabilité industrielle.}

    \begin{flushright}
      Ivan Illich dans \textit{La convivialité}, 1973
    \end{flushright}
  \end{block}

  \begin{block}{}
    \scriptsize
    \textit{La Civilisation des Machines a besoin, sous peine de mort, d'écouler l'énorme production de sa machinerie et elle utilise dans ce but (...) des machines à bourrer le crâne.}
    \vspace{0.2cm}
    \hrule
    \vspace{0.2cm}
    \textit{Le danger n'est pas dans la multiplication des machines, mais dans le nombre sans cesse croissant d'hommes habitués, dès leur enfance, à ne désirer que ce que les machines peuvent donner.}

    \begin{flushright}
      Georges Bernanos dans \textit{La France contre les robots}, 1945
    \end{flushright}
  \end{block}
\end{frame}

\begin{frame}
\vfill
\centering
\begin{beamercolorbox}[sep=8pt,center,shadow=true,rounded=true]{title}
  \usebeamerfont{title}Récupérer les infrastructures\par%
\end{beamercolorbox}
\vfill
\end{frame}

\section{Récupérer les infrastructures}
\subsection{Des fondations à taille humaine}
\begin{frame}
  \centering
  Tout porte à croire que ramener les fondations d'internet au sein de structures à taille plus humaines et aux organisations plus conviviales aiderait à~:\linebreak
  
  \onslide<2->{protéger les utilisateur·rices}\hspace{2cm}\onslide<3->{tout en les responsabilisant}\linebreak

  \onslide<4->{
    \begin{columns}
      \column{0.33 \linewidth}
      \centering
      Cette ligne directrice peut s'appliquer sur bien des axes problématiques du numérique aujourd'hui~:
      \column{0.33 \linewidth}
      \centering
      \begin{itemize}
      \item l'écologie
      \item la modération
      \item l'archivage
      \item les dépendances
      \item etc.
      \end{itemize}
    \end{columns}
  }
\end{frame}

\subsection{Héberger à la maison}
\begin{frame}
  \centering
  Dans cette optique, auto-héberger ses services et ses données présente de nombreux avantages~:

  \vspace{1cm}

  \begin{itemize}
    \setlength\itemsep{0.5cm}
  \item On récupère la souveraineté sur ses données
  \item On gagne en vie privée
  \item On gagne en liberté
  \item On est responsabilisé·e face à ses besoins
  \item On exploite réellement l'énergie consommée par ses serveurs
  \end{itemize}

  \onslide<2->
    \begin{tikzpicture}[remember picture, overlay]
      \node[anchor=center, text width=0.8\textwidth] at (current page.center) {
        \begin{block}{}
          \centering
          En hiver, si on se chauffe électriquement avec un thermostat automatique, l'énergie utilisée par les serveurs est gratuite~!
        \end{block}
      };
    \end{tikzpicture}
\end{frame}

\begin{frame}
  \centering
  Mais cela recouvre également bien des contraintes...

  \vspace{1cm}

  \begin{itemize}
    \setlength\itemsep{0.5cm}
  \item On repose sur une connexion internet pour particulier
  \item Un certain savoir-faire et moultes compétences sont requis
  \item Assurer la résilience de ses services est difficile et chronophage
  \item \color<2->{rose_deuxfleurs}{Bien sauvegarder ses données n'est pas évident}
  \end{itemize}
\end{frame}

\subsection{Sauvegarder pour se parer à tout imprévu}
\begin{frame}
\begin{center}
Sauvegarder pour se parer contre les pannes matérielles est une chose...

Sauvegarder pour se parer contre les cambriolages et les incendies en est une autre !\linebreak

\vspace{1cm}
\onslide<2->{Répartir géographiquement ses données devient alors nécessaire.}
\end{center}
\end{frame}

\subsection{L'entre-hébergement}
\begin{frame}
\begin{center}
On a vu récemment se développer au sein du CHATONS la notion d'\textbf{entre-hébergement} : dans le cadre du collectif, il s'agit de partager ses volumes de données entre hébergeurs.

\vspace{1cm}

En plus de renforcer l'intégrité des sauvegardes, on va améliorer la disponibilité pendant les coupures de liaison internet, de courant, ou pendant les déménagements d'administrateur·rices par exemple.

\vspace{1cm}

Pour assurer la confidentialité, on peut chiffrer les données au niveau applicatif.
\end{center}
\end{frame}

\subsection{S3 contre les systèmes de fichiers}
\begin{frame}
\begin{center}
Dans le cadre de l'administration de services en ligne, les systèmes de fichiers impliquent certaines difficultés.

\vspace{0.5cm}

Le standard S3 apporte des facilités. Il réduit le stockage à un paradigme de clé-valeur basé essentiellement sur deux opérations seulement: lire ou écrire une clé.

\vspace{0.5cm}

\onslide<2->
\begin{itemize}
\item Il permet de s'affranchir de contraintes présentes avec le standard POSIX, notamment sur les nommages et la concurrence
\item Il est pensé pour l'accès à distance
\item Il s'intègre avec le protocole HTTP
\item Il abstrait toute la couche système
\end{itemize}
\end{center}
\end{frame}

\begin{frame}
  \centering
  Historiquement, S3 a été conçu pour être \textit{«\texttt{malloc()} sur internet»}\footnote[frame]{\url{https://aws.amazon.com/fr/blogs/aws/amazon-s3-path-deprecation-plan-the-rest-of-the-story/}}.

  \vspace{2.5cm}

  \onslide<2->
  Très pratique pour l'industrie, ce paradigme participe cependant à la centralisation et l'invisibilisation citées précédemment.
\end{frame}

\begin{frame}
\vfill
\centering
\begin{beamercolorbox}[sep=8pt,center,shadow=true,rounded=true]{title}
  \usebeamerfont{title}Garage\par%
\end{beamercolorbox}
\vfill
\end{frame}

\section{Garage}
\subsection{Présentation}
\begin{frame}
\begin{columns}
\column{0.5 \linewidth}
\centering
Garage essaye de fournir les facilités d'utilisation de S3, mais en conservant une infrastructure accessible derrière.

\vspace{1.5cm}

Il s'agit d'un logiciel libre permettant de distribuer un service S3 sur diverses machines éloignées.
\column{0.5 \linewidth}
\centering
\includegraphics[width=4cm]{ressources/garage-logo.png}\linebreak

\texttt{https://garagehq.deuxfleurs.fr/}
\end{columns}
\end{frame}

\subsection{Gestion des zones}
\begin{frame}
\begin{center}
Garage va prendre en compte les zones géographiques au moment de répliquer les données.\linebreak

\vspace{1cm}
\includegraphics[width=13.25cm]{ressources/zones.png}
\end{center}
\end{frame}

\subsection{Comment ça marche ?}
\begin{frame}
\begin{columns}
\column{0.5 \linewidth}
\input{schéma europe}
\column{0.5 \linewidth}
\begin{center}
Chaque objet est dupliqué sur plusieurs zones différentes.\linebreak

\onslide<5->{Lorsqu'un·e nouvel·le hébergeur·euse rejoint le réseau, la charge se voit équilibrée.}\linebreak

\onslide<12->{Si une zone devient indisponible, les autres continuent d'assurer le service.}\linebreak
\end{center}
\end{columns}
\end{frame}

\subsection{Financement}
\begin{frame}
\begin{center}
Dans le cadre du programme \textit{Horizon 2020} de l'Union Européenne, nous avons reçu une subvention de la part de l'initiative NGI Pointer\footnote[frame]{Next Generation Internet Program for Open Internet Renovation}.\linebreak

\includegraphics[width=3cm]{ressources/drapeau_européen.png}\hspace{1cm}
\includegraphics[width=3cm]{ressources/NGI.png}\linebreak

Nous avons ainsi pu financer le développement de Garage pendant 1 an.
\end{center}
\end{frame}

\subsection{Licence}
\begin{frame}
\begin{center}
De par nos valeurs, nous avons attribué la licence AGPL version 3 à Garage, notamment afin qu'il reste parmi les biens communs.\linebreak

\vspace{0.5cm}
\includegraphics[width=5cm]{ressources/agpl-v3-logo.png}\linebreak
\end{center}
\end{frame}

\subsection{Langage utilisé}
\begin{frame}
\begin{center}
Nous avons décidé d'écrire Garage à l'aide du langage Rust, afin d'obtenir une compilation vers des binaires natifs et efficaces.\linebreak

\includegraphics[width=3.5cm]{ressources/rust-logo.png}\linebreak

Ce choix permet également de bénéficier des avantages reconnus de Rust en termes de sécurité.
\end{center}
\end{frame}

\subsection{Matériel requis}
\begin{frame}
\begin{center}
Garage peut ainsi être performant sur des machines limitées. Les prérequis sont minimes : n'importe quelle machine avec un processeur qui a moins d'une décennie, 1~gigaoctet de mémoire vive, et 16~gigaoctets de stockage suffit.\linebreak

\vspace{1cm}

Cet aspect est déterminant : il permet en effet d'héberger sur du matériel acheté d'occasion, pour réduire l'impact écologique de nos infrastructures.
\end{center}
\end{frame}

\subsection{Performances}
\begin{frame}
\begin{center}
\includegraphics[width=13.25cm]{ressources/rpc-amplification.png}
\end{center}
\end{frame}

\begin{frame}
\begin{center}
\includegraphics[width=11cm]{ressources/rpc-complexity.png}
\end{center}
\end{frame}

\subsection{Services}
\begin{frame}
\begin{center}
Puisqu'il suit le standard S3, beaucoup de services populaires sont par conséquence compatibles avec Garage :\linebreak

\begin{columns}
\column{0.2 \linewidth}
\begin{center}
\includegraphics[width=2.5cm]{ressources/nextcloud-logo.png}
\end{center}
\column{0.2 \linewidth}
\begin{center}
\includegraphics[width=2.5cm]{ressources/peertube-logo.png}
\end{center}
\column{0.2 \linewidth}
\begin{center}
\includegraphics[width=2.5cm]{ressources/matrix-logo.png}
\end{center}
\column{0.2 \linewidth}
\begin{center}
\includegraphics[width=2.5cm]{ressources/mastodon-logo.png}
\end{center}
\end{columns}
~\linebreak

Et comme souvent avec S3, on peut assimiler un bucket à un site, et utiliser le serveur pour héberger des sites web statiques.
\end{center}
\end{frame}

\subsection{Démonstration}
\begin{frame}
  \centering
  \huge
  \underline{\href{ressources/installation_garage.mp4}{Installation simple en 5 minutes}}
\end{frame}

\begin{frame}
\vfill
\centering
\begin{beamercolorbox}[sep=8pt,center,shadow=true,rounded=true]{title}
  \usebeamerfont{title}L'infrastructure de Deuxfleurs\par%
\end{beamercolorbox}
\vfill
\end{frame}

\section{Intégration chez Deuxfleurs}
\subsection{État actuel et ambition}
\begin{frame}
  \centering
  Quid de son exploitation chez Deuxfleurs~?

  \vspace{1.5cm}

  Nous bénéficions d'une grande répartition géographique de nos membres.

  \vspace{1.5cm}

  Et nous visons une offre publique avec un nombre d'usager·ères pour l'instant relativement modeste.
\end{frame}

\subsection{Redondance}
\begin{frame}
  \centering
  \input{schéma redondance}
\end{frame}

\subsection{Matériel}
\begin{frame}
\begin{center}
\includegraphics[width=13cm]{ressources/neptune.jpg}\linebreak

En pratique, nos serveurs ne sont effectivement que des machines achetées d'occasion (très souvent des anciens ordinateurs destinés à la bureautique en entreprise).
\end{center}
\end{frame}

\subsection{Environnement logiciel}
\begin{frame}
\begin{center}
  Pour faciliter la reproduction d'un environnement connu, NixOS est installé sur nos machines.

  \vspace{2cm}

  Cela nous permet de mieux contrôler l'état logiciel de notre parc, et de potentiellement réduire le ticket d'entrée en terme de savoir pour participer à l'infrastructure.
\end{center}
\end{frame}

\subsection{Environnement réseau}
\begin{frame}
  \centering
  Nous fonctionnons beaucoup derrière des routeurs grands publics~: Livebox, Freebox, Bbox...

  \vspace{2cm}
  
  Pour s’accommoder des réseaux qu'on trouve derrière des routeurs pour particuliers, nous nous aidons de notre logiciel Diplonat\footnote[frame]{\texttt{https://git.deuxfleurs.fr/Deuxfleurs/diplonat}}.
\end{frame}

\begin{frame}
\vfill
\centering
\begin{beamercolorbox}[sep=8pt,center,shadow=true,rounded=true]{title}
  \usebeamerfont{title}Le futur\par%
\end{beamercolorbox}
\vfill
\end{frame}

\section{Au-delà...}
\subsection{... de Deuxfleurs}
\begin{frame}
  \begin{center}
    \begin{columns}
      \column{0.7 \linewidth}
      \centering
      \includegraphics[width=8.5cm]{ressources/tedomum.png}
      \column{0.25 \linewidth}
      \centering
      Garage a pris son envol au-delà de Deuxfleurs.

      \vspace{1cm}
      
      Et ceci dans tous types de structures.
    \end{columns}
  \end{center}
\end{frame}

\subsection{... de Garage}
\begin{frame}
\begin{center}
Le développement d'Aerogramme\footnote[frame]{\url{https://aerogramme.deuxfleurs.fr/}} est en cours.

\vspace{0.75cm}

\includegraphics[width=12cm]{ressources/aerogramme.png}

\vspace{0.75cm}

Il s'agit d'un serveur de stockage de courriels chiffrés.

\vspace{0.75cm}

Il est conçu pour reposer sur Garage.
\end{center}
\end{frame}

\section{Fin}
\subsection{Contacts}
{
  \setbeamertemplate{navigation symbols}{}
  \begin{frame}[plain]
    \begin{tikzpicture}[remember picture,overlay]
      \node [anchor=center, at=(current page.center)] {\includegraphics[keepaspectratio, width=\paperwidth]{ressources/ronce.jpg}};
      \node[white] at (1,3) {\Large \textbf{Intéressé·e ?}};
      \node[white, align=center] at (11.75,-4) {\Large Contactez-nous !\\ Par courriel~:~\texttt{coucou@deuxfleurs.fr}\\ Via matrix~:~\texttt{\#forum:deuxfleurs.fr}};
    \end{tikzpicture}
  \end{frame}
}
\end{document}